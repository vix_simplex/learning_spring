package site.vadimanufriev.tutorialspointspring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BeanLifeCycleTests {

    @Autowired
    private ApplicationContext context;

    @Test
    public void postConstructTest() {
        final SomeBean someBean = (SomeBean) context.getBean("prototypeSomeBean");
        assertEquals("default post construct payload", someBean.getPayload());
    }
}
