package site.vadimanufriev.tutorialspointspring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests for understanding bean scope.
 *
 * @see org.springframework.context.annotation.Scope
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BeanScopeTests {

    @Autowired
    private ApplicationContext context;

    /**
     * Test get "singletonSomeBean" - bean with "singleton" scope
     * in two links and checks that links are equals (one bean for two links).
     *
     * @see org.springframework.beans.factory.config.ConfigurableBeanFactory#SCOPE_SINGLETON
     */
    @Test
    public void checkSingletoneBeanScope() {
        final SomeBean link1 = (SomeBean) context.getBean("singletonSomeBean");
        link1.setPayload("kek");

        final SomeBean link2 = (SomeBean) context.getBean("singletonSomeBean");

        assertEquals("kek", link2.getPayload());
        assertEquals(link1, link2);
    }

    /**
     * Test get "prototypeSomeBean" - bean with "prototype" scope
     * in two links and checks that links are different (two different objects for each link).
     *
     * @see org.springframework.beans.factory.config.ConfigurableBeanFactory#SCOPE_PROTOTYPE
     */
    @Test
    public void checkPrototypeBeanScope() {
        final SomeBean link1 = (SomeBean) context.getBean("prototypeSomeBean");
        link1.setPayload("lol");

        final SomeBean link2 = (SomeBean) context.getBean("prototypeSomeBean");

        assertEquals("default post construct payload", link2.getPayload());
        assertNotEquals(link1, link2);
    }

}

