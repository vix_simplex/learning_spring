package site.vadimanufriev.tutorialspointspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialspointspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(TutorialspointspringApplication.class, args);
    }
}

