package site.vadimanufriev.tutorialspointspring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ApplicationConfig {
    
    @Bean(name = "singletonSomeBean")
    @Scope("singleton")
    public SomeBean getSingletonSomeBean() {
        return new SomeBean();
    }

    @Bean(name = "prototypeSomeBean")
    @Scope("prototype")
    public SomeBean getPrototypeSomeBean() {
        return new SomeBean();
    }
}
