package site.vadimanufriev.tutorialspointspring;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Data
public class SomeBean {
    private String payload;

    private static final Logger LOGGER = LoggerFactory.getLogger(SomeBean.class);

    /**
     * @see Bean#initMethod()
     */
    @PostConstruct
    public void init() {
        payload = "default post construct payload";
        LOGGER.info("bean \"SomeBean\" is created");
    }

    /**
     * @see Bean#destroyMethod()
     */
    @PreDestroy
    public void destroy() {
        payload = "default pre destroy payload";
        LOGGER.info("bean \"SomeBean\" will be destroyed after this message.");
    }
}
